use fm_moves;

# --- !Ups

alter table MOVIES add imdb_id INT NULL;
alter table MOVIES add poster_url VARCHAR(1024) NULL;
