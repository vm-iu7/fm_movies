USE fm_movies;

# --- !Ups

CREATE TABLE MOVIES (
  id INT AUTO_INCREMENT NOT NULL,
  name VARCHAR(255) NOT NULL,
  year INT NOT NULL,
  description TEXT NULL,
  PRIMARY KEY(id)
) ENGINE=MyISAM;

CREATE INDEX idx_movies_name ON MOVIES (name);

# --- !Downs

DROP TABLE MOVIES;
DROP INDEX idx_movies_name;
