USE fm_movies;

# --- !Ups

CREATE FULLTEXT INDEX idx_movies_name_ft ON MOVIES(name)
