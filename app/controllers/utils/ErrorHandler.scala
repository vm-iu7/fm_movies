package controllers.utils

import java.io.{PrintWriter, StringWriter}

import play.api.http.HttpErrorHandler
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc.{RequestHeader, Result}

import scala.concurrent.Future


class ErrorHandler extends HttpErrorHandler {
  override def onClientError(request: RequestHeader, statusCode: Int, message: String)
  : Future[Result] = {
    Future.successful(
      Status(statusCode)(Json.obj(
        "error" -> "A client error occurred",
        "details" -> message
      ))
    )
  }

  override def onServerError(request: RequestHeader, exception: Throwable)
  : Future[Result] = {
    val sw = new StringWriter()
    val pw = new PrintWriter(sw)
    exception.printStackTrace(pw)
    Future.successful(
      InternalServerError(Json.obj(
        "error" -> "A server error occurred",
        "details" -> exception.getMessage,
        "stacktrace" -> sw.toString
      ))
    )
  }
}