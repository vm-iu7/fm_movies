package controllers

import javax.inject.Inject

import models.MovieDAO
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import play.api.mvc._


class Movies @Inject() (
  protected val movieDAO: MovieDAO
) extends Controller {

  def get(id: Int) = Action.async {
    movieDAO.lookup(id).map { movieOption =>
      movieOption
        .map(movie => Ok(Json.toJson(movie)))
        .getOrElse(NotFound(Json.obj("error" -> "Resource not found")))
    }
  }

  def getList(ids: List[Int]) = Action.async {
    movieDAO.lookupList(ids.toSet).map { movieList =>
        Ok(Json.toJson(movieList))
    }
  }

  def search(title: String, offset: Int, limit: Int) = Action.async {
    movieDAO.search(title, limit, offset).map { movies =>
      Ok(Json.toJson(movies))
    }
  }

}
