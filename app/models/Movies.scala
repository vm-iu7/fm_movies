package models

import javax.inject.{Inject, Singleton}

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import play.api.libs.json.Json
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}


case class Movie(
  id: Int,
  name: String,
  year: Int,
  description: Option[String] = None,
  tmdbId: Option[Int] = None,
  tmdbPosterPath: Option[String] = None
)

object Movie {
  implicit val jsonFormat = Json.format[Movie]
}

@Singleton
class MovieDAO @Inject() (
  protected val dbConfigProvider: DatabaseConfigProvider
) extends HasDatabaseConfig[JdbcProfile] {

  import driver.api._

  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  private val movies = TableQuery[MoviesTable]

  def lookup(id: Int)
            (implicit ec: ExecutionContext): Future[Option[Movie]] = {
    dbConfig.db.run(
      movies.filter(m => m.id === id).result.headOption
    )
  }

  def lookupList(ids: Set[Int])
                (implicit ec: ExecutionContext): Future[Seq[Movie]] = {
    dbConfig.db.run(movies.filter(_.id inSetBind ids).result)
  }

  def create(name: String, year: Int, description: Option[String])
            (implicit ec: ExecutionContext): Future[Movie] = {
    val r = Movie(0, name, year, description)
    dbConfig.db.run {
      (movies returning movies.map(_.id)) += r
    }.map { id =>
      r.copy(id = id)
    }
  }

  def search(name: String, limit: Int, offset: Int)(implicit ec: ExecutionContext): Future[Seq[Movie]] = {
    dbConfig.db.run {
      sql"""select id, name, year, tmdb_id, tmdb_poster_path
            from MOVIES
            where match (`name`) against ($name) limit $limit offset $offset"""

        .as[(Int, String, Int, Option[Int], Option[String])]
        .map { matchedMovies =>
          matchedMovies.map { case (id: Int, name: String, year: Int, tmdbId: Option[Int], tmdbPosterPath: Option[String]) =>
            Movie(id, name, year, None, tmdbId, tmdbPosterPath)
          }
        }
    }
  }

  def insertAll(data: Iterable[Movie])
               (implicit ec: ExecutionContext): Future[Int] = {
    dbConfig.db
      .run(movies ++= data)
      .map(s => data.size)
  }

  private class MoviesTable(tag: Tag) extends Table[Movie](tag, "MOVIES") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def year = column[Int]("year")
    def description = column[Option[String]]("description")
    def tmdbId = column[Option[Int]]("tmdb_id")
    def tmdbPosterPath = column[Option[String]]("tmdb_poster_path")

    def * = (id, name, year, description, tmdbId, tmdbPosterPath) <> ((Movie.apply _).tupled, Movie.unapply)
  }

}
